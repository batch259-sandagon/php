<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S01: PHP Basics and Selection Control</title>
</head>

<body>
    <!-- <h1>?</h1> -->
    <h1>Echoing Values</h1>
    <p><?php echo'Good day' . $name . '!' . 'Your given email is' . $email . '.'?></p>

    <p> <?php echo PI; ?></p>

    <p><?php echo $headCount; ?></p>

    <h2>Data Types</h2>

    <p>Strings</p>
    <p> <?php echo $address; ?> </p>

    <p>Integers & Float</p>
    <p> <?php echo $age; ?></p>
    <p><?php echo $grade; ?></p>

    <p>Boolean & Null</p>
    <p><?php echo "hasTravelledAbroad: $hasTravelledAbroad"; ?></p>
    <p><?php echo "Spouse: $spouse"; ?></p>

    <!-- gettype() or var_dump -->
    <p><?php echo gettype($hasTravelledAbroad); ?></p>
    <p><?php echo gettype($spouse); ?></p>

    <!-- 
        var_dump function dumps information about one or more variables
     -->
    <p><?php var_dump($hasTravelledAbroad); ?></p>
    <p><?php var_dump($spouse); ?></p>
    <p><?php var_dump($state); ?></p>

    <p>Array</p>
    <p><?php echo var_dump($grades); ?></p>
    <p><?php print_r($grades); ?></p>
    <p><?php echo $grades[3]; ?></p>

    <p>Objects</p>
    <p><?php echo var_dump($personObj); ?></p>
    <p><?php print_r($personObj); ?></p>
    <p><?php echo $gradesObj->firstGrading; ?></p>
    <p><?php echo $personObj->address->state; ?></p>

    <h2>Operators</h2>
    <p>X: <?php echo $x; ?></p>
    <p>Y: <?php echo $y; ?></p>

    <p>Is Legal Age: <?php echo var_dump($isLegalAge); ?></p>
    <p>Is Registered: <?php echo var_dump($isRegistered); ?></p>

    <h2>Arithmetic Operators</h2>
    <p>Sum: <?php echo $x + $y; ?></p>
    <p>Difference: <?php echo $x - $y; ?></p>
    <p>Product: <?php echo $x * $y; ?></p>
    <p>Quotient: <?php echo $x * $y; ?></p>
    <p>Modulo: <?php echo $x % $y; ?></p>

    <h2>Equality Operators</h2>
    <p>Loose Equality: <?php echo var_dump($x == "500") ?></p>
    <p>Strict Equality: <?php echo var_dump($x === "500") ?></p>

    <p>Loose Inequality: <?php echo var_dump($x != "500") ?></p>
    <p>Strict Inequality: <?php echo var_dump($x !== "500") ?></p>

    <h2>Greater/Lesser Operators</h2>
    <p>Is Lesser: <?php echo var_dump($x < $y) ?></p>
    <p>Is Greater: <?php echo var_dump($x > $y) ?></p>

    <p>Is Lesser or Equal: <?php echo var_dump($x <= $y) ?></p>
    <p>Is Greater or Equal: <?php echo var_dump($x >= $y) ?></p>

    <h2>Logical Operators</h2>
    <p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered) ?></p>

    <p>Are Some Requirements Met: <?php echo var_dump($isLegalAge || $isRegistered); ?> </p>

    <p>Are Some Requirements Not Met: <?php echo var_dump(!$isLegalAge && !$isRegistered); ?> </p>

    <h2>Functions:</h2>

    <h2>Selection Control Structures</h2>
    <h3>If-Else-if-Else</h3>

    <p><?php echo determineTyphoonIntensity(12) ?></p>

    <h2>Ternary Sample (Is Underage?)</h2>
    <p>25: <?php var_dump(isUnderAge(25)) ?>;</p>
    <p>17: <?php var_dump(isUnderAge(17)) ?>;</p>

    <h2>Switch</h2>
    <p><?php determineComputerUser(4); ?></p>

    <h2>Try-Catch-Finally</h2>
    <p><?php greeting("Hello"); ?></p>
    <p><?php greeting(1); ?></p>

</body>

</html>