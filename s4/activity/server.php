<?php
session_start();

if (isset($_SESSION['username'])) {
    echo "Logged in as {$_SESSION['username']} <br>";
    echo '
    <form method="post" action="">
        <input type="submit" name="logout" value="Logout">
    </form>';
} else {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $username = $_POST['username'];
        $password = $_POST['password'];

        if ($username === 'jhonsmith@gmail.com' && $password === '1234') {
            $_SESSION['username'] = $username;

            header('Location: server.php');
            exit;
        } else {
            echo 'Incorrect username or password';
        }
    }

    echo '
    <form method="post" action="">
    <label>Username:</label>
    <input type="text" name="username" required>
    <label>Password:</label>
    <input type="password" name="password" required>
    <input type="submit" value="Login">
    </form>';
}

if (isset($_POST['logout'])) {
    session_destroy();
    header('Location: index.php');
    exit;
}