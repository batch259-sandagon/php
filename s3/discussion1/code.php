<?php

// Objects as Variables
$buildingObj = (object) [
    "name" => "Caswyn Building",
    "floor" => 8,
    "address" => "Brgy. Sacred Heart, Quezon City, Philippines"
];

$buildingObj2 = (object) [
    "name" => "GMA Network",
    "floor" => 20,
    "address" => "Brgy. Sacred Heart, Quezon City, Philippines"
];

// Objects from Classes
class Building {
    // Properties (instead of variables)
    public $name;
    public $floor;
    public $address;

    // Contructor Function
    // A constructor allows us to initialization an object's properties upon creation of the object
    public function __construct($name, $floor, $address) {
        // Characteristics of an Object
        $this->name = $name;
        $this->floor = $floor;
        $this->address = $address;
    }

    // Methods 
    // An action that object can perform
    public function printName() {
        return "The name of the building is $this->name.";
    }
    
};

// Inheritance and Polymorphism
// Inheritance - The derive classes are allowed to inherit properties and methods from a specified base class.
    // The "extends" keyword is used to derive a class from another class.
    // A derive/class class has all the properties and methods of the parent class.
    // PARENT CLASS => BUILDING
    // CHILD CLASS => CONDOMINIUM
class Condominium extends Building {
    // Polymorphism - MEthods inehrited by a child/derived class can be overriden t ohave a behavior different from the method of the base/parent class
    public function printName() {
        return "The name of the building is $this->name.";
    }
};

$building = new Building('Caswynn Building', 8,'Timog Ave., Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avuenue, Makati City, Philippines');