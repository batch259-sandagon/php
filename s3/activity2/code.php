<?php

class Building {
    
    protected $name;
    protected $floor;
    protected $address;

    public function __construct($name, $floor, $address) {
        $this->name = $name;
        $this->floor = $floor;
        $this->address = $address;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        if (is_string($name)) {
            $this->name = $name;
        }
    }

    public function getFloor() {
        return $this->floor;
    }

    public function setFloor($floor) {
        if (is_int($floor)) {
            $this->floor = $floor;
        }
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        if (is_string($address)) {
            $this->address = $address;
        }
    }
    
};

class Condominium extends Building {
    // Inherits getters and setters from Building
};

$building = new Building("Default Building", 0, "No Address");
$condominium = new Condominium("Default Condominium", 0, "No Address");