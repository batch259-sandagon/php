<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03B: Access Modifiers and Encapsulation</title>
</head>

<body>

    <h1>Building</h1>
    <?php $building->setName("Caswynn Building") ?>
    <p><?php echo "The name of the building is " . $building->getName() . "."; ?></p>
    <?php $building->setFloor(8) ?>
    <p><?php echo "The " . $building->getName() . " has " . $building->getFloor() . " floors."; ?></p>
    <?php $building->setAddress("Timog Avenue, Quezon City, Philippines") ?>
    <p><?php echo "The " . $building->getName() . " is located at " . $building->getAddress() . "."; ?></p>
    <p><?php echo "The name of the building has been changed to " . $building->getName() . "."; ?></p>

    <h1>Condominium</h1>
    <?php $condominium->setName("Enzo Condo") ?>
    <p><?php echo "The name of the building is " . $condominium->getName() . "."; ?></p>
    <?php $condominium->setFloor(5) ?>
    <p><?php echo "The " . $condominium->getName() . " has " . $condominium->getFloor() . " floors."; ?></p>
    <?php $condominium->setAddress("Buendia Avenue, Makati City, Philippines") ?>
    <p><?php echo "The " . $condominium->getName() . " is located at " . $condominium->getAddress() . "."; ?></p>
    <p><?php echo "The name of the building has been changed to " . $condominium->getName() . "."; ?></p>

</body>

</html>