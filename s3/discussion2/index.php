<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03B: Access Modifiers and Encapsulation</title>
</head>

<body>

    <h2>Access Modifiers</h2>

    <h2>Building Variables</h2>
    <p><?php print_r($building); ?></p>
    <p><?php // echo $building->name; ?></p>

    <h2>Condominium Variable</h2>
    <p><?php print_r($condominium); ?></p>
    <p><?php // echo $condominium->name; ?></p>


    <h2>Encapsulation</h2>

    <h2>Condominium Variable</h2>
    <p><?php print_r($condominium); ?></p>

    <!-- GETTER -->
    <p>The name of the condominium is <?php echo $condominium->getName() ?></p>

    <!-- SETTER -->
    <?php $condominium->setName("Enzo Tower") ?>
    <p>The name of the condominium is <?php echo $condominium->getName() ?></p>


</body>

</html>