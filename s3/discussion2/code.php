<?php

class Building {

    // Access Modifiers
    // 1. public: fully open, property and methods can be accessed from anywhere
    // 2. private
        // disables direct access to an object's property or methods
        // private modifier also disables inheritance of its properties and methods to child class
        // method and property can only be accessed within the class
    // 3. protected:
        // this modifier will allow inheritance of properties and methods to child classes
        // properties or method is only accessible within the class and child class
    
    protected $name;
    protected $floor;
    protected $address;

    public function __construct($name, $floor, $address) {
        // Characteristics of an Object
        $this->name = $name;
        $this->floor = $floor;
        $this->address = $address;
    }
    
};

class Condominium extends Building {
    // getter (retriever) and setter (mutator)
    // This is use to retrieve and modilfy the values
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        if (gettype($name) === "string") {
            $this->name = $name;
        }
    }
};

$building = new Building('Caswynn Building', 8,'Timog Ave., Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avuenue, Makati City, Philippines');